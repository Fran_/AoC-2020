#!/usr/bin/env python3


def ordenar_por_dígito(a, i):
    B = [[None]] * 10
    for num in a:
        dígito = (num // (10 ** i)) % 10
        if B[dígito] == [None]:
            B[dígito] = [num]
        else:
            B[dígito].append(num)
    lista_res = []
    for lista in B:
        if lista == [None]:
            continue
        else:
            for num in lista:
                lista_res.append(num)

    return lista_res


def radix_sort_int(arr):
    for i in range(0, 10):
        arr = ordenar_por_dígito(arr, i)
    return arr


def buscar_2020():
    # Leo el txt
    lista_advent = [line.strip() for line in open("AoC1.txt")]
    # Viene como [str], hay que hacerlo [int].
    lista_advent = [int(num) for num in lista_advent]
    # Ordeno con radix sort:
    lista_advent = radix_sort_int(lista_advent)
    # 2 Índices para buscar los elementos que suman 2020.
    i = 0
    j = len(lista_advent) - 1
    lista_res = []
    while i < j:
        # Si los índices actuales suman más de 2020, nos
        # pasamos, como tenemos una lista ordenada, tenemos
        # que buscar a la izquierda del índice j.
        if lista_advent[i] + lista_advent[j] > 2020:
            j -= 1
        # El opuesto al caso anterior
        elif lista_advent[i] + lista_advent[j] < 2020:
            i += 1
        else:
            lista_res.append((i, j))

    return (
        lista_advent[i] + lista_advent[j],
        (i, j),
        lista_advent[i],
        lista_advent[j],
        lista_advent[i] * lista_advent[j],
    )


print(buscar_2020())
