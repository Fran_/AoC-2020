# /usr/bin/env python3
import re as regex


def parsear_contraseñas(lista):
    lista_parseada = []
    for contraseña in lista:
        # Matcheo los límites
        limite_inf = regex.findall("^(.*?)-", contraseña)
        limite_sup = regex.findall("\-(.*)\w\:", contraseña)
        # Matcheo TODOS los char
        contraseña_a_testear_validez = regex.findall("[a-z]", contraseña)
        # El primer char siempre va a ser el requerido.
        # Pop elimina y devuelve
        caracter_requerido = contraseña_a_testear_validez.pop(0)
        # Agrego todo lo que encontré a la lista que devuelvo.
        lista_parseada.append(
            (int(limite_inf[0]), int(limite_sup[0]), caracter_requerido,
             contraseña_a_testear_validez))
    return lista_parseada


def contraseñas_válidas():
    # Leo el txt
    lista_advent = [line.strip() for line in open("AoC2.txt")]
    lista_advent = parsear_contraseñas(lista_advent)
    nro_contraseñas_validas = 0
    for tupla in lista_advent:
        min = tupla[0]
        max = tupla[1]
        char_actual = tupla[2]
        contador = 0
        for caracter in tupla[3]:
            contador += 1 if caracter == char_actual else 0
        nro_contraseñas_validas += 1 if (contador <=
                                         max and contador >= min) else 0
