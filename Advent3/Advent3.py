#!/usr/bin/env python3
import functools
from functools import reduce


def advent3():
    # Básicamente, el mapa es una matriz.
    matriz_advent = [line.strip() for line in open("Advent3.txt")]
    # Me quedo con la altura, así sé cuándo tengo que dejar de contar.
    altura = len(matriz_advent)
    # Tengo en cuenta el largo de una fila, así
    # sé cuándo tengo que volver a posicionarme en la matriz.
    limite_fila = len(matriz_advent[0])
    # Empiezo en (0,0).
    i = 0
    j = 0
    árboles = 0
    while i < altura:
        if matriz_advent[i][j] == '#':
            árboles += 1
        # Avanzamos a la derecha 3 unidades, y como me puedo "pasar",
        # tomo módulo. Tenemos limte_fila columnas, así que avanzo módulo esta
        # cantidad.
        j = (j+3) % limite_fila
        # Avanzo para abajo:
        i += 1
    return árboles


def advent3_bis():
    # Básicamente, el mapa es una matriz.
    matriz_advent = [line.strip() for line in open("Advent3.txt")]
    # Me quedo con la altura, así sé cuándo tengo que dejar de contar.
    altura = len(matriz_advent)
    # Tengo en cuenta el largo de una fila, así
    # sé cuándo tengo que volver a posicionarme en la matriz.
    limite_fila = len(matriz_advent[0])
    # Empiezo en (0,0).
    i = 0
    j = 0
    árboles = 0
    pendientes = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]
    árboles_por_pendiente = []
    for pendiente in pendientes:
        while i < altura:
            if matriz_advent[i][j] == '#':
                árboles += 1
            j = (j+pendiente[0]) % limite_fila
            i += pendiente[1]
        árboles_por_pendiente.append(árboles)
        árboles = 0
        i = 0
        j = 0
    # Producto de toda la lista
    return reduce(lambda x, y: x * y, árboles_por_pendiente, 1)


print(advent3_bis())
